package memoryAllocation

import "fmt"


type Example struct {
	Name *string
}

func Exercise1() {
	a := 1
	b := "hello world"
	fmt.Println(a, b)
}

func Exercise2() {
	integersSlice := []int{1, 2, 3, 4}
	stringsSlice := []string{"one", "two", "three"}

	fmt.Println(integersSlice)
	fmt.Println(stringsSlice)
}

func Exercise3() {
	nestedSlice := [][]interface{}{
		{1.2, 3.4, 5.6},
		{"apple", "orange", "banana"},
		{7.8, "grape", 0.9},
	}

	for i, innerSlice := range nestedSlice {
		fmt.Printf("Inner Slice %d: ", i)
		for _, value := range innerSlice {
			fmt.Printf("%v\t", value)
		}
		fmt.Println()
	}
}

func Exercise4() {
	var exampleString string
	var exampleRune rune

	stringPointer := &exampleString
	runePointer := &exampleRune

	fmt.Print("Enter a string: ")
	fmt.Scanln(stringPointer)

	fmt.Print("Enter a rune: ")
	fmt.Scanf("%c", runePointer)

	fmt.Printf("Value of string using pointer: %s\n", *stringPointer)
	fmt.Printf("Value of rune using pointer: %c\n", *runePointer)
}

func Exercise5() {
	exampleName := "example"
	exampleInstance := Example{Name: &exampleName}
	fmt.Println(*exampleInstance.Name)
}

func Exercise6() {
	siema := "siema"
	dupa := "dupa"
	elo := "elo"
	dwa := "dwa"
	gówno := "gówno"

	examplesSlice := []*Example{
		{Name: &siema},
		{Name: &dupa},
		{Name: &elo},
		{Name: &dwa},
		{Name: &gówno},
	}

	for _, example := range examplesSlice {
		fmt.Println(*example.Name)
	}
}