package stringManipulation

import (
	"strings"
)

func ReverseString(s string) string {
	runes := []rune(s)

	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}

	reversedString := string(runes)
	return reversedString
}

func IncrementLetters(s string) string {
	runes := []rune(s)

	for index := range runes {
		if (runes[index] >= 'a' && runes[index] < 'z') || (runes[index] >= 'A' && runes[index] < 'Z') {
			runes[index] += 1
		} else if runes[index] == 'z' {
			runes[index] = 'a'
		} else if runes[index] == 'Z' {
			runes[index] = 'A'
		}
	}

	incrementedString := string(runes)
	return incrementedString
}

func MakeTitle(s string) string {
	return strings.Title(s)
}

func GetLongestWord(s string) string {
	words := strings.Fields(s)

	longest := ""
	for _, word := range words {
		if len(word) > len(longest) {
			longest = word
		}
	}
	return longest
}

func SortLetters(s string) []string {
	sorted := make([]string, 0, len(s))

Loop:
	for _, char := range s {
		currentChar := string(char)
		if len(sorted) == 0 {
			sorted = append(sorted, currentChar)
		} else {
			for index, letter := range sorted {
				if currentChar < letter {
					sorted = append(sorted[:index], append([]string{currentChar}, sorted[index:]...)...)
					continue Loop
				}
			}
			sorted = append(sorted, currentChar)
		}
	}
	return sorted
}
