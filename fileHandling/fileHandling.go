package fileHandling

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
)

func NewTextFile(filename, text string) error {
	f, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = f.Write([]byte(text))
	if err != nil {
	  return err
	}

	return nil
}

func PrintTextFile(filename string) error {
	content, err := os.ReadFile(filename)
	if err != nil {
	  return err
	}

	fmt.Println(string(content))
	return nil
}

func CountLines(filename string) error {
	file, err := os.Open(filename)
	if err != nil {
	  return err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)

	var count int
	for scanner.Scan() {
		count++
	}
	if err := scanner.Err(); err != nil {
		return err
	}
	fmt.Println(count)
	return nil
}

func CountWords(filename string) error {
	file, err := os.Open(filename)
	if err != nil {
	  return err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanWords)

	var count int

	for scanner.Scan() {
		count++
	}
	fmt.Println(count)
	return nil
}

func CopyTextToFile(filenameToCopy, filenameToPaste	string) error {
	content, err := os.ReadFile(filenameToCopy)
	if err != nil {
	  return err
	}

	file, err := os.OpenFile(filenameToPaste, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
	  return err
	}
	defer file.Close()

	content = append(content, []byte("\n")...)
	_, err = file.Write(content)
	if err != nil {
	  return err
	}
	return nil
}

func ReplaceWord(filename, wordToBeReplaced, wordToReplace string) error {
	content, err := os.ReadFile(filename)
	if err != nil {
	  return err
	}
	replaced := bytes.Replace(content, []byte(wordToBeReplaced), []byte(wordToReplace), -1)
	file, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
	  return err
	}
	defer file.Close()

	_, err = file.Write(replaced)
	if err != nil {
	  	return err
	}
	return nil
}

func AppendToFile(filename, text string) error {
	file, err := os.OpenFile(filename, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
	  	return err
	}
	defer file.Close()

	_, err = file.Write([]byte(text))
	if err != nil {
		return err
	}

	return nil
}