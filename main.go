package main

import (
	"exercisesBasics/memoryAllocation"
	"exercisesBasics/stringManipulation"
	"exercisesBasics/fileHandling"
	"exercisesBasics/structsAndInterfaces"
	"fmt"
)

func main() {
	memoryAllocation.Exercise1()
	memoryAllocation.Exercise2()
	memoryAllocation.Exercise3()
	// memoryAllocation.Exercise4()
	memoryAllocation.Exercise5()
	memoryAllocation.Exercise6()
	stringToReverse := "string to reverse"
	stringToReverse = stringManipulation.ReverseString(stringToReverse)
	fmt.Println(stringToReverse)
	stringToIncrement := "string to increment"
	stringToIncrement = stringManipulation.IncrementLetters(stringToIncrement)
	fmt.Println(stringToIncrement)
	stringToTitle := "string to title"
	stringToTitle = stringManipulation.MakeTitle(stringToTitle)
	fmt.Println(stringToTitle)
	stringToGetLongest := "string to get longest string"
	longest := stringManipulation.GetLongestWord(stringToGetLongest)
	fmt.Println(longest)
	stringToSort := "stringtosort"
	sorted := stringManipulation.SortLetters(stringToSort)
	fmt.Println(sorted)
	fileHandling.NewTextFile("fileHandling/textFiles/textfile.txt", "This is a text file")
	fileHandling.PrintTextFile("fileHandling/textFiles/textfile.txt")
	fileHandling.CountLines("fileHandling/textFiles/textfile.txt")
	fileHandling.CountWords("fileHandling/textFiles/textfile.txt")
	fileHandling.CopyTextToFile("fileHandling/textFiles/textfile.txt", "fileHandling/textFiles/textfile2.txt")
	fileHandling.ReplaceWord("fileHandling/textFiles/textfile2.txt", "text", "replaced text")
	fileHandling.AppendToFile("fileHandling/textFiles/textfile1.txt", "appended text\n")
	car := structsAndInterfaces.InitCar()
	car.SetFields("Audi", "A4", 2011)
	car.GetField("Company")
	car.GetField("Model")
	car.GetField("Year")

	bankAccount := &structsAndInterfaces.BankAccount{
		AccountNumber: "312 2312 3231 13 213 1321",
		Balance: 0}
	bankAccount.Deposit(100)
	bankAccount.Withdraw(50)
	fmt.Println(bankAccount.Balance)
	triangle := &structsAndInterfaces.Triangle{A: 1,B: 2,C: 3}
	fmt.Println(triangle.IsEquilateral())
	fmt.Println(triangle.IsIsosceles())
	fmt.Println(triangle.IsScalene())
	employee := &structsAndInterfaces.Employee{
		Name: "John",
		EmployeeID: 1,
		Salary: 10000}
	newSalary := employee.CalculateSalary(63) 
	employee.SetSalary(newSalary)
	fmt.Println(employee.Salary)
	date := &structsAndInterfaces.Date{}
	date.SetDate(1, 1, 2020)
	date.GetDate()
	student := &structsAndInterfaces.Student{
		Name: "Andrzej Stopka",
		Class: 4,
		RollNumber: 1231,
		Marks: []int{1,2,3,4,5,2,2,1,2,3,1,5},
	}
	student.CalculateGrade()
	student.Info()
}