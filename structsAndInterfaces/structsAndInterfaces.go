package structsAndInterfaces

import (
	"fmt"
	"reflect"
)

type Car struct {
	Company string
	Model string
	Year int
}

func InitCar() Car {
	return Car{}
}

func (c *Car) SetFields(company, model string, year int) {
	c.Company = company
	c.Model = model
	c.Year = year
}

func (c *Car) GetField(field string) error {
	r := reflect.ValueOf(c)
	f := reflect.Indirect(r).FieldByName(field)

	if !f.IsValid() {
		return fmt.Errorf("field %s not valid", field)
	}

	fmt.Println(f.Interface())
	return nil
}


type BankAccount struct {
	AccountNumber string
	Balance float64
}

func (ba *BankAccount) Deposit(deposit float64) {
	ba.Balance += deposit
}

func (ba *BankAccount) Withdraw(withdraw float64) {
	ba.Balance -= withdraw
}

type Triangle struct {
	A, B, C float64
}

func (t *Triangle) IsEquilateral() bool {
	return t.A == t.B && t.C == t.A
}

func (t *Triangle) IsIsosceles() bool {
		return t.A == t.B || t.A == t.C || t.B == t.C
}

func (t *Triangle) IsScalene() bool {
	return t.A != t.B && t.A != t.C && t.B != t.C 
}

type Employee struct {
	Name string
	EmployeeID int
	Salary float64
}

func (e *Employee) SetSalary(newSalary float64) {
	e.Salary = newSalary
}

func (e *Employee) CalculateSalary(performance int) float64 {
	if performance < 30 {
		return e.Salary * 0.6
	} else if performance < 50 {
		return e.Salary * 0.8
	} else if performance < 75 {
		return e.Salary * 1.15
	} else if performance < 90 {
		return e.Salary * 1.7
	} else {
		return e.Salary * 2
	}
}

type Date struct {
	Day int
	Month int
	Year int
}

func (d *Date) SetDate(day, month, year int) {
	if day < 1 || day > 31 || month < 1 || month > 12 {
		panic("Wrong date")
	}
	d.Day = day
	d.Month = month
	d.Year = year
}

func (d *Date) GetDate() {
	fmt.Printf("%d/%d/%d\n", d.Day, d.Month, d.Year)
}

type Student struct {
	Name string
	Class int
	RollNumber int
	Marks []int 
	Grade float64
}

func (s *Student) CalculateGrade() {
	sum := 0
	for _, mark := range s.Marks {
		sum += mark
	}
	s.Grade = float64(sum) / float64(len(s.Marks))
}

func (s *Student) Info() {
	fmt.Print(
		"\n",
		"Name: ", s.Name, "\n",
		"Class: ", s.Class, "\n",
		"Roll Number: ", s.RollNumber, "\n",
		"Marks: ", s.Marks, "\n",
		"Grade: ", s.Grade, "\n",
	)
}


